import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import gnu.getopt.Getopt;


/** ========================================
 *  Additional import class/package
 *  ======================================== */
import java.util.BitSet;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Base64.Encoder;


public class DES extends SBoxes
{
    /* Global Variables */
    private static StringBuilder privKey;
    private static BitSet IV;
    private static BitSet[] aBlock;
    
    public static void main(String[] args)
    {

        StringBuilder inputFile = new StringBuilder();
        StringBuilder outputFile = new StringBuilder();
        StringBuilder keyStr = new StringBuilder();
        StringBuilder encrypt = new StringBuilder();

        pcl(args, inputFile, outputFile, keyStr, encrypt);

        if(keyStr.toString() != "" && encrypt.toString().equals("e"))
        {
            encrypt(keyStr, inputFile, outputFile);
        }
        else if(keyStr.toString() != "" && encrypt.toString().equals("d"))
        {
            decrypt(keyStr, inputFile, outputFile);
        }

    }


    private static void decrypt(StringBuilder keyStr, StringBuilder inputFile, StringBuilder outputFile)
    {
        try
        {
            PrintWriter writer = new PrintWriter(outputFile.toString(), "UTF-8");
            List<String> lines = Files.readAllLines(Paths.get(inputFile.toString()), Charset.defaultCharset());
            String IVStr = lines.get(0);
            lines.remove(0);
            
            
            privKey = keyStr;
            
            String encryptedText;

            for(String line : lines)
            {
                encryptedText = DES_decrypt(IVStr, line);
                writer.print(encryptedText + "\n");
            }
            writer.close();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }

    }

    /**
     * TODO: You need to write the DES decryption here.
     * @param line
     */
    private static String DES_decrypt(String IVStr, String line)
    {
        IV = new BitSet(64);
        /* Copy binaries into BitSet */
        for(int i = 0; i < 64; i++)
        {
            IV.set(i, IVStr.charAt(i) == '1');
        }
        
        System.out.println("IV:\n" + bitSetToString(IV) + "\n");
        System.out.println("Input:");
        int blocks = line.length() / 64;
        aBlock = new BitSet[blocks];
        
        for(int i = 0; i < blocks; i++)
        {
            aBlock[i] = new BitSet(64);
            for(int j = 0; j < 64; j++)
            {
                aBlock[i].set(j, line.charAt(i * 64 + j) == '1');
            }
            System.out.println(bitSetToString(aBlock[i]));
        }
        System.out.println("\nXOR:");
        
        String result = "";
        BitSet[] decodeBlocks = new BitSet[blocks];

        
        //********** TODO:
        /* Key expansion */
        /* Goal: String privKey -> BitSet(48) keyTable[16] */
        /* Step 1: String privKey -> BitSet keyBits */
        BitSet keyBits = new BitSet(56);
        keyStringToBitSet(keyBits, privKey.toString());
        
        /* Step 2: BitSet keyBits -> BitSet(48) keyTable[16] */
        /* Key expansion */
        BitSet[] keyTable = new BitSet[16];
        
        for(int i = 0; i < 16; i++)
        {
            /* Initialize keyTable[i] */
            keyTable[i] = new BitSet(48);
            
            keyTable[i] = (BitSet)KeyExpansion(keyBits).clone();
        }
        
        
        
        for(int i = 0; i < blocks; i++)
        {

            decodeBlocks[i] = new BitSet(64);
            decodeBlocks[i] = (BitSet)aBlock[i].clone();
            
            /* Initial permutation */
            IPermutations(decodeBlocks[i]);
            
            
            /* Feistel network */
            for(int x = 0; x < 16; x++)
            {
                //********** TODO:
                // Feistel(decodeBlocks[i], keyTable[i]);
                Feistel(decodeBlocks[i], keyTable[i]);
            }
            Swap(decodeBlocks[i]);
            
            
            /* Final permutation */
            FPermutations(decodeBlocks[i]);
            
            System.out.println("Final permuration:\n" + bitSetToString(aBlock[i]));
            
            if(i == 0)
            {
                decodeBlocks[i].xor(IV);
            }
            else
            {
                decodeBlocks[i].xor(aBlock[i - 1]);
            }
        }
        
        for(int i = 63; i > 63 - 8; i--)
        {
            decodeBlocks[blocks - 1].clear(i);
        }
        for(int i = 0; i < blocks; i++)
        {
            System.out.println(bitSetToString(decodeBlocks[i]));
            result += binaryToString(bitSetToString(decodeBlocks[i]));
        }
        return result;
    }
    
    
 

    private static void encrypt(StringBuilder keyStr, StringBuilder inputFile, StringBuilder outputFile)
    {

        try
        {
            PrintWriter writer = new PrintWriter(outputFile.toString(), "UTF-8");

            /* Initial key value to global from input */
            privKey = keyStr;
            
            String encryptedText;
            
            
            IV = new BitSet(64);
            do
            {
                generateIV(IV);
            }
            while(haveNL( bitSetToString(IV) ));
            
            System.out.println("IV:\n" + bitSetToString(IV) + "\n");
            /* Write IV into output file */
            writer.print(bitSetToString(IV) + "\n");
            
            for(String line : Files.readAllLines(Paths.get(inputFile.toString()), Charset.defaultCharset()))
            {
                encryptedText = DES_encrypt(line);
                
                writer.print(encryptedText + "\n");
                
            }
            writer.close();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }
    
    
    public static boolean haveNL(String binStr)
    {
        for(int i = 0; i < 64; i += 8)
        {
            if(binaryToChar(binStr.substring(i, i + 8)) == '\n')
            {
                return true;
            }
        }
        return false;
    }
    
    /** ========================================
     *  generateIV(BitSet)
     *  ----------------------------------------
     *  Generate the initial vector.
     *  ======================================== */
    public static void generateIV(BitSet bits)
    {
        SecureRandom rand = new SecureRandom();
        for(int i = 0; i < 64; i++)
        {
            if(rand.nextBoolean())
            {
                bits.set(i);
            }
        }
    }
    
    /**
     * TODO: You need to write the DES encryption here.
     * @param line
     */
    private static String DES_encrypt(String line)
    {
        StringBuilder output = new StringBuilder();
        
        /* Convert original String into a repersented binary String */
        String binaryStr = stringToBinary(line);
        
        int blocks = binaryStr.length() / 64 + 1;
        int remain = binaryStr.length() % 64;
        
        // test print out of blocks needed and remained that already filled
        //System.out.println(binaryStr);
        //System.out.println("blocks: " + blocks + "\nremain: " + remain);

        aBlock = new BitSet[blocks];
        
        System.out.println("Original:");
        for(int i = 0; i < blocks; i++)
        {
            aBlock[i] = new BitSet(64);
            
            /* Chop 64 bits off from the original binaryStr */
            String tmp64;
            if(i * 64 + 64 < binaryStr.length())
            {
                tmp64 = binaryStr.substring(i * 64, i * 64 + 64);
            }
            else /* Last block */
            {
                tmp64 = binaryStr.substring(i * 64);
            }

            // Test print out original bits
            System.out.println(tmp64);
            
            /* Copy binaries into BitSet */
            for(int j = 0; j < 64; j++)
            {
                if(j < tmp64.length())
                {
                    aBlock[i].set(j, tmp64.charAt(j) == '1');
                }
            }
        }
        
        /* Fill up the tail of the last block with remain marker */
        fillTail(aBlock[blocks - 1], (char)remain); // Important!
        
        
      
        // Test print out of the final BitSet array
        System.out.println("\nProcessed:");
        for(int i = 0; i < blocks; i++)
        {
            System.out.println(bitSetToString(aBlock[i]));
            
            //System.out.println(binaryToString(bitSetToString(aBlock[i])));
        }
        
        
        
        
        
        //********** TODO:
        /* Key expansion */
        /* Goal: String privKey -> BitSet(48) keyTable[16] */
        /* Step 1: String privKey -> BitSet keyBits */
        BitSet keyBits = new BitSet(56);
        keyStringToBitSet(keyBits, privKey.toString());
        
        /* Step 2: BitSet keyBits -> BitSet(48) keyTable[16] */
        /* Key expansion */
        BitSet[] keyTable = new BitSet[16];

        for(int i = 0; i < 16; i++)
        {
            /* Initialize keyTable[i] */
            keyTable[i] = new BitSet(48);
            
            keyTable[i] = (BitSet)KeyExpansion(keyBits).clone();
        }
        
        
    
        
        
        System.out.println("\nXOR:");
        String result = "";
        
        for(int i = 0; i < blocks; i++)
        {
        
            if(i == 0)
            {
                aBlock[i].xor(IV);
            }
            else
            {
                aBlock[i].xor(aBlock[i - 1]);
            }
            System.out.println(bitSetToString(aBlock[i]));
            
            /* Initial permutation */
            IPermutations(aBlock[i]);
            
            /* Feistel network */
            for(int x = 0; x < 16; x++)
            {
                //********** TODO:
                Feistel(aBlock[i], keyTable[i]);
            }
            Swap(aBlock[i]);
            
            /* Final permutation */
            FPermutations(aBlock[i]);
            
            System.out.println("Final permuration:\n" + bitSetToString(aBlock[i]));
            
            result += bitSetToString(aBlock[i]);
        }
        
        return result;
    
    }
    
    /** ========================================
     *  KeyExpansion(BitSet)
     *  ----------------------------------------
     *  Modify the key and generate a 48
     *  ======================================== */
    private static BitSet KeyExpansion(BitSet theKey)
    {
        //System.out.println("===============\nOriginal:");
        //PrintBitSet(theKey, 56);
        
        
        BitSet left = new BitSet(28);
        BitSet right = new BitSet(28);
        
        /* Split */
        for(int i = 0; i < 28; i++)
        {
            left.set(i, theKey.get(i));
            right.set(i, theKey.get(i + 28));
        }
        //System.out.println("===============\nOriginal part:");
        //PrintBitSet(left, 28);
        //PrintBitSet(right, 28);
        
        /* Shift */
        shiftPosition(left, 1);
        shiftPosition(right, 1);
        
        //System.out.println("===============");
        //PrintBitSet(left, 28);
        //PrintBitSet(right, 28);
        
        /* Refresh theKey */
        for(int i = 0; i < 28; i++)
        {
            theKey.set(i, left.get(i));
            theKey.set(i + 28, right.get(i));
        }

        PrintBitSet(theKey, 56);
        
        BitSet clone = new BitSet(28);
        clone = (BitSet)theKey.clone();

        /* PC-2 */
        PC2(clone);
        
        return clone;
    }
    
    
    private static void PC2(BitSet bits)
    {
        BitSet cloneBits = (BitSet)bits.clone();
        for(int j = 0; j < 48; j++)
        {
            bits.set(j, cloneBits.get(PC2[j]-1));
        }
    }
    
    private static void shiftPosition(BitSet bits, int value)
    {
        //System.out.println("===============\nOriginal:");
        //PrintBitSet(bits, 28);
        
        BitSet clone = new BitSet(28);
        clone = (BitSet)bits.clone();
        
        boolean[] tmp = new boolean[value];
        for(int i = 0; i < tmp.length; i++)
        {
            tmp[i] = clone.get(i);
        }
        int i;
        for(i = 0; i < bits.length() - value; i++)
        {
            bits.set(i, clone.get(i + value));
        }
        
        for(int j = 0; j < tmp.length; j++)
        {
            bits.set(i, tmp[j]);
            i++;
        }
        
        
        //System.out.println("Shifted:");
        //PrintBitSet(clone, 28);
        //PrintBitSet(bits, 28);
    }
    
    
    
    /** ========================================
     *  keyStringToBitSet(BitSet, String)
     *  ----------------------------------------
     *  Convert input string into BitSet(56) and
     *  replace the original input BitSet.
     *  ======================================== */
    private static void keyStringToBitSet(BitSet keyBits, String keyStr)
    {
        /* Input check */
        if(keyStr.length() != 16)
        {
            // System.err.println("* keyStringToBitSet: key length is not 16.");
            System.err.println("* Error: key inputed is invalid.");
            System.exit(-1); // User level error occured.
        }
        
        String keyBin = new BigInteger(keyStr, 16).toString(2);
        System.out.println("Key:\n" + keyBin);
        
        /* Fill up leading 0s */
        while(keyBin.length() < 64)
        {
            keyBin = "0" + keyBin;
        }
        
        System.out.println(keyBin + "\n");
        
        /* Bit size check */
        if(keyBin.length() > 64)
        {
            System.err.println("* keyStringToBitSet: key size is over 64 bit.");
            System.exit(-2); // Error occured.
        }
        
        for(int i = 0; i < 56; i++)
        {
            if(keyBin.charAt(i) == '1')
            {
                keyBits.set(i);
            }
        }
        //PrintBitSet(keyBits);
    }
    
    private static void PrintBitSet(BitSet bits, int size)
    {

        for(int i = 0; i < size; i++)
        {
            if(bits.get(i))
            {
                System.out.print("1");
            }
            else
            {
                System.out.print("0");
            }
        }
        System.out.println(" <<PrintBitSet>>");
    }
    
    
    
    /** ========================================
     *  Feistel(BitSet)
     *  ----------------------------------------
     *  Feistel network
     *  ======================================== */
    private static void Feistel(BitSet bits, BitSet key)
    {
        BitSet left = new BitSet(32);
        BitSet right = new BitSet(32);
        
        for(int i = 0; i < 32; i++)
        {
            left.set(i, bits.get(i));
            right.set(i, bits.get(i + 32));
        }
        
        BitSet rightTemp = new BitSet(32);
        rightTemp = (BitSet)right.clone();
        
        rightTemp.xor(function(key)); // Function f
        left.xor(rightTemp);
        
        for(int i = 0; i < 32; i++)
        {
            bits.set(i, right.get(i));
            bits.set(i + 32, left.get(i));
        }
        
    }
    private static void Swap(BitSet bits)
    {
        BitSet left = new BitSet(32);
        BitSet right = new BitSet(32);
        
        for(int i = 0; i < 32; i++)
        {
            left.set(i, bits.get(i));
            right.set(i, bits.get(i + 32));
        }
        
        for(int i = 0; i < 32; i++)
        {
            bits.set(i, right.get(i));
            bits.set(i + 32, left.get(i));
        }
    }
    
    
    // TODO HERE:
    /** ========================================
     *  function(BitSet, BitSet)
     *  ----------------------------------------
     *  This is the f function.
     *  ======================================== */
    //private static BitSet function(BitSet bits, BitSet keyBits)
    private static BitSet function(BitSet key)
    {
        return key;
    }
    
    
    /** ========================================
     *  IPermutations(BitSet) & FPermutations(BitSet)
     *  ----------------------------------------
     *  Initial permutation & final permutation
     *  ======================================== */
    private static void IPermutations(BitSet bits)
    {
        BitSet cloneBits = (BitSet)bits.clone();
        for(int j = 0; j < 64; j++)
        {
            bits.set(j,cloneBits.get(IP[j]-1));
        }
    }
    private static void FPermutations(BitSet bits)
    {
        BitSet cloneBits = (BitSet)bits.clone();
        for(int j = 0; j < 64; j++)
        {
            bits.set(j,cloneBits.get(FP[j]-1));
        }
    }

    
    
    
    /** ========================================
     *  fillTail(BitSet, String)
     *  ----------------------------------------
     *  Fill in the tail with binary value of
     *  the input char.
     *  ======================================== */
    public static void fillTail(BitSet bits, char aChar)
    {
        String binary = charToBinary(aChar);
        // For test purpose, be ware of the length must <= 8.
        //binary = "1011000111";
        //System.out.println("To int: " + binaryToInt(binary));

        int backTraversal = 63;
        for(int i = binary.length() - 1; i >= 0; i--)
        {
            if(binary.charAt(i) == '1')
            {
                bits.set(backTraversal);
            }
            
            backTraversal--;
        }
    }
    
    
    /** ========================================
     *  bitSetToString(BitSet)
     *  ----------------------------------------
     *  Convert a 64 bit BitSet into a binary
     *  String.
     *  ======================================== */
    public static String bitSetToString(BitSet bits)
    {
        String bitStr = "";
        for(int i = 0; i < 64; i++)
        {
            if(bits.get(i))
            {
                bitStr += "1";
            }
            else
            {
                bitStr += "0";
            }
        }
        return bitStr;
    }
    
    
    
    /** ========================================
     *  stringToBinary(String)
     *  ----------------------------------------
     *  Convert a String into a binary String.
     *  ======================================== */
    public static String stringToBinary(String str)
    {
        char[] chars = str.toCharArray();
        
        StringBuilder string = new StringBuilder();
        
        for(char c:chars)
        {
            String binary = charToBinary(c);
            // Turn this on would result in error.
            //System.out.println("<<< " + binaryToChar(binary) + " >>>\n");
            
            // Test print out before formatted
            //System.out.println("--- " + binary + " ---");
            
            /* Formatted into 8 bits. */
            for(int i = 0; i < 8 - binary.length() + 1; i++)
            {
                binary = "0" + binary;
            }
            
            // Test print out after formatted
            //System.out.println("*** " + binary + " ***\n");
            //System.out.println("<<< " + binaryToChar(binary) + " >>>\n");
            
            string.append(binary);
        }
        return string.toString();
    }
    
    
    /** ========================================
     *  binaryToString(String)
     *  ----------------------------------------
     *  Convert a binary String into a String.
     *  ======================================== */
    public static String binaryToString(String binaryStr)
    {
        /* Checking binaryStr length */
        if(binaryStr.length() != 64)
        {
            System.err.println("* binaryToString: Input binary String length was not 64.");
            System.exit(-2); // Error occured.
        }
        
        String output = "";
        for(int i = 0; i < 64; i += 8)
        {
            output += binaryToChar(binaryStr.substring(i, i+8));
        }
        
        return output;
    }
    
    /** ========================================
     *  binaryToChar(String)
     *  ----------------------------------------
     *  Convert a String of a 8-bit binary
     *  into a char.
     *  ======================================== */
    public static char binaryToChar(String str)
    {
        if(str.length() > 8)
        {
            System.err.println("* binaryToChar: Serious error occured on bit size.");
            System.exit(-2); // Error occured.
        }
        return (char)Integer.parseInt(str, 2);
    }
    
    /** ========================================
     *  binaryToInt(String)
     *  ----------------------------------------
     *  Convert a String of a 8-bit binary
     *  into an int.
     *  ======================================== */
    public static int binaryToInt(String str)
    {
        if(str.length() > 8)
        {
            System.err.println("* binaryToInt: Serious error occured on bit size.");
            System.exit(-2); // Error occured.
        }
        return Integer.parseInt(str, 2);
    }

    /** ========================================
     *  charToBinary(String)
     *  ----------------------------------------
     *  Convert a char into a String of a 8-bit
     *  binary.
     *  ======================================== */
    public static String charToBinary(char aChar)
    {
        return Integer.toBinaryString((int)aChar);
    }
    
    /** ========================================
     *  genDESkey()
     *  ----------------------------------------
     *  Generate a random 64-bit hex key.
     *  ======================================== */
    static void genDESkey()
    {
        System.out.print("New 64-bit hex key generated.\nYour key: ");
        
        SecureRandom rand = new SecureRandom();
        int randNum;
        String key = "ffffffffffffffff";

        while(isWeakKey(key, 0))
        {
            key = "";
            for(int i = 0; i < 16; i++)
            {
                do
                {
                    randNum = rand.nextInt() % 16;
                }
                while(randNum < 0);
                
                key += Integer.toHexString(randNum);
            }
        }
        System.out.println(key);
    }
    
    /** ========================================
     *  isWeakKey(String, int)
     *  ----------------------------------------
     *  return:
     *      true: if input is a weak key.
     *      false: if input is not a weak key.
     *  ======================================== */
    static boolean isWeakKey(String key, int index)
    {
        if(index < 8)
        {
            String tmp = key.substring(index * 2, index * 2 + 2);
            boolean checker = false;
            for(int i = 0; i < 8; i++)
            {
                if(i != index)
                {
                    checker = checker || tmp.equals(key.substring(i * 2, i * 2 + 2));
                }
            }
            return checker || isWeakKey(key, index + 1);
        }
        else
        {
            return false;
        }
    }


    /**
     * This function Processes the Command Line Arguments.
     * -p for the port number you are using
     * -h for the host name of system
     */
    private static void pcl(String[] args, StringBuilder inputFile, StringBuilder outputFile, StringBuilder keyString, StringBuilder encrypt)
    {
        /*
         * http://www.urbanophile.com/arenn/hacking/getopt/gnu.getopt.Getopt.html
         */
        Getopt g = new Getopt("Chat Program", args, "hke:d:i:o:");
        int c;
        String arg;
        while((c = g.getopt()) != -1)
        {
            switch(c)
            {
                case 'o':
                    arg = g.getOptarg();
                    outputFile.append(arg);
                    break;
                case 'i':
                    arg = g.getOptarg();
                    inputFile.append(arg);
                    break;
                case 'e':
                    arg = g.getOptarg();
                    keyString.append(arg);
                    encrypt.append("e");
                    break;
                case 'd':
                    arg = g.getOptarg();
                    keyString.append(arg);
                    encrypt.append("d");
                    break;
                case 'k':
                    genDESkey();
                    break;
                case 'h':
                    callUsage(0);
                // Unknow command
                case '?':
                    // getopt() already printed an error
                    
                    // return -1 for unknow command
                    callUsage(-1); // User level error occured.
                    break;
                default:
                    break;
            }
        }

    }

    
    /** ========================================
     *  callUsage(int)
     *  ----------------------------------------
     *  input: exit status code
     *  Will print usage and exit program with
     *  given exit status.
     *  ======================================== */
    private static void callUsage(int exitStatus)
    {

        String usage =  "    -h: help and usage\n" +
                        "    -k: this will generate a 64 bit DES hex key\n" +
                        "    -e <64_bit_key_in_hex> -i <input_file> -o <output_file>: encrypt\n" +
                        "    -d <64_bit_key_in_hex> -i <input_file> -o <output_file>: decrypt";
        
        System.err.println(usage);
        System.exit(exitStatus);

    }

}
