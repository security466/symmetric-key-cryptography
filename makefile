#!/bin/bash
all: compile mkalias

#For compile:
compile:
	javac -cp ".:./getopt.jar" DES.java

#Make alias:
mkalias:
	alias DES="java \-cp \".:./getopt.jar\" DES"

clean:
	rm -f *.class